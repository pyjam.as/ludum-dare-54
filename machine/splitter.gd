extends "res://machine/machine.gd"

var waiting_items = 0

var items_to_move = []
var item_scene = load("res://Item/item.tscn")


func reset_state():
	items_on_the_way = 0
	items_to_move = []
	waiting_items = 0

func eject(item):
	if items_to_move.is_empty():
		for new_dish in item.dish.split():
			var new_item = item.duplicate()
			new_item.dish = new_dish
			item.get_parent().add_child(new_item)
			items_to_move.append(new_item)
		item.queue_free()
	
	for output_node in output_nodes:
		if not items_to_move.is_empty():
			if output_node.has_space():
				item = items_to_move[0]
				items_to_move.erase(item)
				move_item_to(item, output_node.global_position, output_node.global_scale)
