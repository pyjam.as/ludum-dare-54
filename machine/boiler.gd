extends "res://machine/machine.gd"

var cooking_time = 2
var is_cooking = false
var is_finished = false
var time_left = 0

var cooked_name = "boiled"

func reset_state():
	is_cooking = false
	is_finished = false
	time_left = 0
	items_on_the_way = 0

func move_and_cook(item, dest_pos, dest_scale):
	var cooked_item = item.duplicate()
	item.get_parent().add_child(cooked_item)
	cooked_item.dish = item.dish.affect(cooked_name)
	item.queue_free()
	print(cooked_item.dish)
	move_item_to(cooked_item, dest_pos, dest_scale)

func eject(item):
	if is_finished:
		if output_nodes[0].has_space():
			move_and_cook(item, output_nodes[0].global_position, output_nodes[0].global_scale)
			is_finished = false
	else:
		if not is_cooking:
			is_cooking = true
			time_left = cooking_time
		elif time_left > 0:
			time_left -= 1
		else:
			is_cooking = false
			if output_nodes[0].has_space():
				move_and_cook(item, output_nodes[0].global_position, output_nodes[0].global_scale)
				is_finished = false
			else:
				is_finished = true
