extends Node2D

@export var outputs = 1 
@export var do_rotate = true
var square_size = 100.
var margin_scale = 0.2
var output_nodes = []
@export var max_items = 1
var items_on_the_way = 0

var is_ready_to_receive = false


func output_chosen(event):
	pass

func set_output_selector_label_text(outputs_left):
	var label = get_node("/root/Root/ClickBlocker/CanvasLayer/Label")
	label.text = "SELECT " + str(outputs_left) + " OUTPUT SQUARE"
	if outputs_left > 1: 
		label.text += "S"

func choose_outputs():
	output_nodes = []
	var buttons = []
	var neighbors_list = neighbors()
	var clickblocker = get_node("/root/Root/ClickBlocker")
	
	clickblocker.visible = true
	for neighbor in neighbors_list:
		var button = load("res://select_output_button/select_output_button.tscn").instantiate()
		clickblocker.add_child(button)
		button.global_scale = neighbor.global_scale * 5
		var neighbor_rect = neighbor.get_node("ColorRect")
		button.global_position = neighbor_rect.global_position
		button.global_position += (neighbor_rect.size * neighbor.global_scale.x) / 2
		button.neighbor = neighbor
		buttons.append(button)
	is_ready_to_receive = true
		
	# wait for N outputs to be chosen
	for x in range(outputs):
		set_output_selector_label_text(outputs - x)
		var next_output = await Events.output_chosen
		output_nodes.append(next_output)

	# clean up
	clickblocker.visible = false
	for button in buttons:
		if not is_deleted(button):
			button.queue_free()
	
	# place indicators on this machine
	remove_output_arrows()
	place_output_arrows()
	unalert_deleted_outputs()
	rotate_sprite()

func rotate_sprite():
	if not do_rotate:
		return
	if len(output_nodes) != 1:
		return
	var output_node = output_nodes[0]
	var vec = output_node.global_position - global_position
	if abs(vec.x) > abs(vec.y):
		if vec.x > 0:
			# right
			# conveyor already faces right
			$Sprite2D.rotation = 0
		if vec.x < 0:
			# left
			$Sprite2D.rotation = PI
	else:
		if vec.y > 0:
			$Sprite2D.rotation = PI/2
		if vec.y < 0:
			$Sprite2D.rotation = -PI/2

func remove_output_arrows():
	for child in get_children():
		if child.is_in_group("output_arrows"):
			child.queue_free()

func place_output_arrows():
	for output_node in output_nodes:
		place_output_arrow(output_node)

func place_output_arrow(output_node: Node2D):
	# make sprite
	var sprite = Sprite2D.new()

	sprite.texture = load("res://images/arrow.png")
	var width_scale = output_node.global_scale[0] * margin_scale * 8
	sprite.scale = Vector2(width_scale, margin_scale)
	sprite.add_to_group("output_arrows")
	sprite.modulate = Color(1,0.2,0)
	add_child(sprite)

	var sprite_width = sprite.texture.get_width() * sprite.global_scale.x
	var sprite_height = sprite.texture.get_height() * sprite.global_scale.y
	var square_width = global_scale.x*square_size

	# figure out where
	var span = output_node.global_position - self.global_position
	if abs(span.x) > abs(span.y):
		# is either to the left or the right
		var sprite_y = output_node.global_position.y if output_node.global_scale.x < global_scale.x else global_position.y
		if span.x > 0:
			# to the right
			var right_edge = global_position.x + (square_width/2)
			var sprite_x = right_edge - (sprite_height/2)
			sprite.global_position = Vector2(sprite_x, sprite_y)
			sprite.rotate(-PI/2)
		if span.x < 0:
			# to the left
			var left_edge = (global_position.x - (square_width/2))
			var sprite_x = left_edge + (sprite_height/2)
			sprite.global_position = Vector2(sprite_x, sprite_y)
			sprite.rotate(PI/2)
	if abs(span.y) > abs(span.x):
		var sprite_x = output_node.global_position.x if output_node.global_scale.y < global_scale.y else global_position.x
		if span.y > 0:
			# below
			var bottom_edge = global_position.y + (square_width/2)
			var sprite_y = bottom_edge - (sprite_height / 2)
			sprite.global_position = Vector2(sprite_x, sprite_y)
			sprite.rotate(0)
		if span.y < 0:
			# above
			var top_edge = global_position.y - (square_width/2)
			var sprite_y = top_edge + (sprite_height / 2)
			sprite.global_position = Vector2(sprite_x, sprite_y)
			sprite.rotate(PI)

func alert_deleted_outputs():
	$DeletedOutputsAlert.visible = true

func unalert_deleted_outputs():
	$DeletedOutputsAlert.visible = false

func neighbors():
	var detection_radius = $detection_radius
	var areas: Array = detection_radius.get_overlapping_areas()
	areas.erase(get_parent())
	var grid_squares = areas.filter( func(area): return area.is_in_group("grid_square"))
	return grid_squares

func handle(item):
	eject(item)
	# TODO HACK this should be run as a separate tick, so we know that all squares have finished their tick before resetting stuff
	post_tick()
	
func post_tick():
	items_on_the_way = 0

func neighbor(dir):
	pass

func eject(item):
	if output_nodes.is_empty(): return
	# temp solution does not account for multiple outputs
	var output_node = output_nodes[0]
	if output_node.has_space():
		move_item_to(item, output_node.global_position, output_node.global_scale)
		#item.global_position = output_node.global_position
		#item.global_scale = output_node.global_scale
		
func move_item_to(item: Node, dest_pos: Vector2, dest_scale):
	# move and scale the whole node
	dest_scale *= 1.5
	
	var sprite = item.get_node("Sprite2D")
	
	var old_position = item.global_position
	var old_scale = item.global_scale
	var old_sprite_scale = sprite.global_scale
	
	
	item.global_position = dest_pos
	item.global_scale = dest_scale
	
	
	# ..but leave the sprite behind
	sprite.global_position = old_position
	sprite.global_scale = old_sprite_scale
	
	
	var sprite_scale_factor = old_sprite_scale / old_scale
	
	var dest_scale_sprite = sprite_scale_factor * dest_scale
	
	var duration = 0.2 # XXX: this should be less than a tick
	
	# tween to catch up position
	var tween = get_tree().create_tween()
	tween.tween_property(sprite, "global_position", dest_pos, duration).set_trans(Tween.TRANS_QUINT).set_ease(Tween.EASE_IN_OUT)
	
	# tween to catch up scale
	var tween_scale = get_tree().create_tween()
	tween_scale.tween_property(sprite, "global_scale", dest_scale_sprite, duration).set_trans(Tween.TRANS_QUINT).set_ease(Tween.EASE_IN_OUT)

func reset_state():
	items_on_the_way = 0


func has_space():
	if not is_ready_to_receive:
		return false
	else:
		if len(get_parent().items) + items_on_the_way < max_items:

			items_on_the_way += 1
			return true
		else:
			return false

func is_deleted(node):
	var wr = weakref(node)
	return !wr.get_ref()

func has_deleted_output():
	for output_node in output_nodes:
		if is_deleted(output_node):
			return true
	return false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if has_deleted_output():
		alert_deleted_outputs()
