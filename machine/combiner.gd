extends "res://machine/machine.gd"

var waiting_items = 0

var items_to_move = []
var waiting_to_eject = false

func reset_state():
	is_ready_to_receive = true
	items_on_the_way = 0
	items_to_move = []
	waiting_items = 0
	waiting_to_eject = false

func combine_items(item1, item2):
	var combined_item = item1.duplicate()
	combined_item.dish = item1.dish.combine(item2.dish)
	item1.get_parent().add_child(combined_item)
	item1.queue_free()
	item2.queue_free()
	return combined_item
	
func eject(item):
	var items = get_parent().items
	if waiting_to_eject:
		if output_nodes[0].has_space():
			move_item_to(item, output_nodes[0].global_position, output_nodes[0].global_scale)
			waiting_to_eject = false
			#is_ready_to_receive = true
	elif len(items) > 1:
		var combined_item = combine_items(items[0], items[1])
		waiting_to_eject = true
		#is_ready_to_receive = false
		if output_nodes[0].has_space():
			#is_ready_to_receive = true
			move_item_to(combined_item, output_nodes[0].global_position, output_nodes[0].global_scale)
	
	
