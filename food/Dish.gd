class_name Dish

extends Object

var Food = load("res://food/Food.gd")
var food_items = []

func _to_string():
	var result = ""
	for food in food_items:
		result += str(food)
		result += " | "
	return result

func combine(dish2: Dish) -> Dish:
	print("combining")
	var dish1: Dish = self
	var result = Dish.new()
	for food1 in dish1.food_items:
		var found_food1 = false
		for food2 in dish2.food_items:
			if food1.equals(food2):
				found_food1 = true
				var combined_food: Food = food1.combine(food2)
				result.food_items.append(combined_food)
		if not found_food1:
			result.food_items.append(food1)
	
	for food2 in dish2.food_items:
		var found_food2 = false
		for food1 in dish1.food_items:
			if food2.equals(food1):
				found_food2 = true
		if not found_food2:
			result.food_items.append(food2)

	return result

func half() -> Dish:
	var result = Dish.new()
	for food in food_items:
		result.food_items.append(food.half())
	return result

func split() -> Array:
	return [self.half(), self.half()]

func affect(attribute: String) -> Dish:
	var result = Dish.new()
	for food in food_items:
		var newfood = food.copy()
		newfood.attributes.append(String(attribute))
		newfood.attributes.sort()
		result.food_items.append(newfood)
	return result

func copy() -> Dish:
	var result = Dish.new()
	for food in food_items:
		var newfood = food.copy()
		result.food_items.append(newfood)
	return result

func equals(otherDish: Dish) -> bool:
	if not len(otherDish.food_items) == len(food_items):
		return false
	for food1 in food_items:
		var food1found = false
		for food2 in food_items:
			if food1.equals(food2) and food1.amount == food2.amount:
				food1found = true
				break
		if not food1found:
			return false

	for food2 in food_items:
		var food2found = false
		for food1 in food_items:
			if food1.equals(food2):
				food2found = true
		if not food2found:
			return false
	return true
