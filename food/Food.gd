class_name Food

extends Object

var ingredient: String
var attributes = []
var amount = 0

func _to_string():
	return str(amount) + "g of " + " ".join(attributes) + " " + ingredient

func equals(otherFood: Food) -> bool:
	return (
		(ingredient == otherFood.ingredient) and
		same_attributes(otherFood)
	)

func same_attributes(otherFood: Food) -> bool:
	for attribute in attributes:
		if not (attribute in otherFood.attributes):
			print("no1")
			return false
			
	for otherAttr in otherFood.attributes:
		if not (otherAttr in attributes):
			print("no2")
			return false
	return true 

func combine(otherFood: Food) -> Food:
	var result = Food.new()
	assert(self.equals(otherFood))
	result.ingredient = self.ingredient
	result.attributes = self.attributes
	result.amount = self.amount + otherFood.amount
	return result

func half() -> Food:
	var result = Food.new()
	result.ingredient = String(ingredient)
	result.attributes = attributes.duplicate(true)
	result.amount = amount / 2
	return result

func copy() -> Food:
	var result = Food.new()
	result.ingredient = String(ingredient)
	result.attributes = attributes.duplicate(true)
	result.amount = amount
	return result
