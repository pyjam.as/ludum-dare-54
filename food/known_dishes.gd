
static func string_to_dish(dish_name: String) -> Dish: 
	if dish_name == "kig_of_rice":
		return kig_of_rice()
	if dish_name == "kig_of_fish":
		return kig_of_fish()
	return dubious_food()

static func dubious_food() -> Dish:
	var result = Dish.new()
	var dubious_food = Food.new()
	dubious_food.ingredient = "dubious food"
	result.food_items.append(dubious_food)
	return result

static func kig_of_rice() -> Dish:
	var result = Dish.new()
	var rice = Food.new()
	rice.ingredient = "rice"
	rice.amount = 1024
	result.food_items.append(rice)
	return result

static func kig_of_potato() -> Dish:
	var potato = Food.new()
	potato.ingredient = "potato"
	potato.amount = 1024
	var result = Dish.new()
	result.food_items.append(potato)
	return result

static func kig_of_fish() -> Dish:
	var fish = Food.new()
	fish.ingredient = "fish"
	fish.amount = 1024
	var result = Dish.new()
	result.food_items.append(fish)
	return result

static func kig_of_carrot() -> Dish:
	var carrot = Food.new()
	carrot.ingredient = "carrot"
	carrot.amount = 1024
	var result = Dish.new()
	result.food_items.append(carrot)
	return result


static func dish_to_texturepath(dish: Dish) -> String:
	var dishstr = str(dish)
	if dishstr.begins_with("1024g of  rice |"):
		return "res://images/rasterized/dish_rice.svg.png"
	print("UNKNOWN DISH")
	print(dishstr)
	return "res://images/mandag/dish_wtf.png"
