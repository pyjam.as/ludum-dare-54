extends Camera2D

func zoom_at_point(zoom_change, point):
	var span = Vector2(point.x - global_position.x, point.y - global_position.y)
	var move_by = span * (1-(1/(zoom_change)))
	if zoom_change < 1:
		move_by *= -1
	global_position += move_by
	zoom *= zoom_change

func _unhandled_input(event):
	if event.is_action_pressed("zoom_in"):
		zoom_at_point(1.1, get_global_mouse_position())
	elif event.is_action_pressed("zoom_out"):
		# zoom_at_point(1/1.1, get_global_mouse_position())
		zoom /= 1.1
