extends Control

const LEVEL_BTN = preload("res://level_selector/lvl_btn.tscn")
@export_dir var dir_path = "res://levels/"

@onready var grid = $MarginContainer/VBoxContainer/GridContainer

func _ready() -> void:
	get_levels(dir_path)

func get_levels(path = "res://levels/"):
	var dir = DirAccess.open(path)
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		print(file_name)
		create_level_btn('%s/%s' % [dir.get_current_dir(), file_name], file_name)
		file_name = dir.get_next()
	dir.list_dir_end()

func create_level_btn(lvl_path, lvl_name):
	var btn = LEVEL_BTN.instantiate()
	var name = lvl_name.trim_suffix('.remap') # for web
	btn.text = name.trim_suffix('.tscn').replace("_", " ")
	btn.level_path = lvl_name
	grid.add_child(btn)
