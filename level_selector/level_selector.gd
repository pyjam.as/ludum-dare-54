extends Control

const LEVEL_BTN = preload("res://level_selector/lvl_btn.gd")
@export_dir var dir_path = "res://levels"

@onready var grid = $MarginContainer/VBoxContainer/GridContainer

func _ready() -> void:
	get_levels(dir_path)

func get_levels(path):
	var dir = DirAccess.open(path)
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		print(file_name)
		create_level_btn('%s/%s' % [dir.get_current_dir(), file_name], file_name)
		file_name = dir.get_next()
	dir.list_dir_end()

func create_level_btn(lvl_path, lvl_name):
	var btn = LEVEL_BTN.instantiate()
	btn.text = lvl_name.trim_suffix('.tscn').replace("_", " ")
	btn.level_path = lvl_path
	grid.add_child(btn)
