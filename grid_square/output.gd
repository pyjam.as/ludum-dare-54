extends "res://grid_square/grid_square.gd"

var known_dishes = load("res://food/known_dishes.gd")
var items_consumed = {}

# TODO set these dynamically
var target_dish: Dish
@export var target_count = 1
var correct_count = 0

@export var target_dish_name = "kig_of_rice"

func _ready():
	Events.connect("tick", _on_tick)
	target_dish = known_dishes.string_to_dish(target_dish_name)
	$Label.text = str(target_dish)
	$CountLabel.text = str(correct_count) + " / " + str(target_count)

func show_menu():
	hide_all_other_menus()
	$Menu.visible = false
	if Events.can_tick: return
	# choose which buttons to show.
	for child in $Menu/HBox.get_children():
		child.visible = false

func _on_tick():
	for item in items:
		consume(item)
		
func has_space():
	return true

func consume(item):
	items_consumed[item] = item
	if item.dish.equals(target_dish):
		correct_count += 1
		$CountLabel.text = str(correct_count) + " / " + str(target_count)
		if correct_count == target_count:
			get_parent().get_node("Control").visible = true
			Events.can_tick = false
	item.queue_free()
	items.erase(item)
