extends Area2D

@onready var grid_scene = load("res://grid/grid.tscn")
var items = []
var color = 1.0

var items_on_the_way = 0

func _ready():
	Events.connect("tick", _on_tick)

func reset_state():
	items = []
	if has_node("machine"):
		$machine.reset_state()

func _on_tick():
	if has_node("machine"):
		if not items.is_empty():
			$machine.handle(items[0])
	items_on_the_way = 0

func subdivide():
	var grid_instance = grid_scene.instantiate()

	# set position and size
	grid_instance.scale = self.scale * 0.5
	grid_instance.position = self.position
	
	# and to parent at delete self
	self.get_parent().add_child(grid_instance)
	self.queue_free()

func add_machine(machine_name = "machine"):
	var machine_scene = load("res://machine/"+machine_name+".tscn").instantiate()
	add_child(machine_scene)
	move_child(machine_scene, 2) # < XXX put child after ColorRect, but before Menu
	await get_tree().create_timer(0.1).timeout
	machine_scene.choose_outputs()
	items = self.get_overlapping_areas().filter(func(item): return item.is_in_group("item"))

func show_menu():
	hide_all_other_menus()
	if Events.can_tick: return
	$Menu.visible = true
	# choose which buttons to show.
	if has_node("machine"):
		# show these
		$Menu/HBox/DeleteMachine.visible = true
		$Menu/HBox/SetOutputs.visible = true
		# dont show these
		$Menu/HBox/Subdivide.visible = false
		$Menu/HBox/Conveyor.visible = false
		$Menu/HBox/Boiler.visible = false
		$Menu/HBox/Splitter.visible = false
		$Menu/HBox/Combiner.visible = false
	else:
		# show these
		$Menu/HBox/Subdivide.visible = true
		$Menu/HBox/Conveyor.visible = true
		$Menu/HBox/Boiler.visible = true
		$Menu/HBox/Splitter.visible = true
		$Menu/HBox/Combiner.visible = true
		# dont show these
		$Menu/HBox/DeleteMachine.visible = false
		$Menu/HBox/SetOutputs.visible = false

func hide_all_other_menus():
	for square in get_tree().get_nodes_in_group("grid_square"):
		if square != self:
			square.hide_menu()
	for grid in get_tree().get_nodes_in_group("grid"):
		grid.get_node("Control").hide_menu()

func hide_menu():
	$Menu.visible = false

func toggle_highlight(highlight_on):
	if highlight_on: $Sprite2D.modulate = Color("ffffff", 0.1)
	else: $Sprite2D.modulate = Color("ffffff", 1)

func _on_mouse_entered():
	# TODO: make sure this doesn't trigger on touch events
	#       (it currently does)
	toggle_highlight(true)

func _on_mouse_exited():
	toggle_highlight(false)

func is_leftclick(event):
	return event is InputEventMouseButton and event.button_index == 1 and event.pressed

func is_touchtap(event):
	return event is InputEventScreenTouch

func is_click(event):
	return is_touchtap(event) or is_leftclick(event)

func _on_input_event(viewport, event, shape_idx):
	if is_click(event):
		show_menu()

func _on_splitter_gui_input(event):
	if is_click(event):
		add_machine("splitter")
		hide_menu()

func _on_boiler_gui_input(event):
	if is_click(event):
		add_machine("boiler")
		hide_menu()

func _on_subdivide_gui_input(event):
	if is_click(event):
		subdivide()
		hide_menu()

func _on_conveyor_gui_input(event):
	if is_click(event):
		add_machine()
		hide_menu()

func _on_delete_machine_gui_input(event):
	if is_click(event): # Replace with function body.
		$machine.queue_free()
		hide_menu()
		
func _on_combiner_gui_input(event):
	if is_click(event):
		add_machine("combiner")
		hide_menu()
		
func has_space():
	if has_node("machine"):
		return get_node("machine").has_space()
	elif len(items) + items_on_the_way < 1:
		items_on_the_way += 1
		return true

func _on_set_outputs_gui_input(event):
	if is_click(event):
		$machine.choose_outputs()
		hide_menu()

# global signal that calls handle on all items in list.
# on area adds to list
func _on_area_entered(area):
	if area.is_in_group("item"):
		items.append(area)

func _on_area_exited(area):
	if area.is_in_group("item"):
		items.erase(area)
