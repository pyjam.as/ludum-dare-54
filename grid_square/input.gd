extends "res://grid_square/grid_square.gd"

@export var item: PackedScene
var Dish = load("res://food/Dish.gd")
var Food = load("res://food/Food.gd")
var known_dishes = load("res://food/known_dishes.gd")

@export var generate_dish_name = "kig_of_rice"
var generate_dish = null

func _ready():
	generate_dish = known_dishes.string_to_dish(generate_dish_name)
	await get_tree().create_timer(0.1).timeout
	var nodes = $machine.neighbors()
	$machine.output_nodes.append(nodes[0])
	Events.connect("tick", _on_tick)
	$machine.do_rotate = false
	$Sprite2D.visible=false

func show_menu():
	hide_all_other_menus()
	if Events.can_tick: return
	$Menu.visible = true
	# choose which buttons to show.
	for child in $Menu/HBox.get_children():
		child.visible = false
	$Menu/HBox/SetOutputs.visible = true

func _on_tick():
	if items.is_empty():
		generate_item()
	else:
		if has_node("machine"):
			for item in items:
				# handle every child instaed of conveyor
				$machine.handle(item)

func generate_item():
	if not items.is_empty(): return # dont put add more items if already filled  
	var item_scene = item.instantiate()
	item_scene.dish = generate_dish.copy()
	add_child(item_scene)
