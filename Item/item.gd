extends Area2D

var Dish = load("res://food/Dish.gd")
var dish: Dish = null
var known_dishes = load("res://food/known_dishes.gd")

func _physics_process(delta):
	if dish != null:
		var path = known_dishes.dish_to_texturepath(dish)
		$Sprite2D.texture = load(path)
		$Label.text = str(dish)

func _on_mouse_entered():
	$Label.visible = true


func _on_mouse_exited():
	$Label.visible = false
