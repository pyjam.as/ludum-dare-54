
# Ludum Dare 54 Submission

I don't even know what the theme is yet, so this is just an example setup repo with some CI and stuff.

## Install nix

We use nix flakes to build the game. This is the same environment that runs in gitlab ci.

Install `nix` on your distro of choice - remember to enable `flakes` and `nix-command`.
The easiest way is probably to just add it to `/etc/nix/nix.conf` like [this](https://nixos.wiki/wiki/Flakes).

## Run the game

`nix build` will build the game, and put result in `./result`.

`nix run` will serve the game on `https://localhost:8443`
