extends Node2D

var counter = 0
var tick_length = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	counter += delta
	if counter > tick_length:
		counter -= tick_length
		Events.emit_signal("tick")
