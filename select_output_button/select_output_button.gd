extends Area2D

var neighbor: Node2D

func is_leftclick(event):
	return event is InputEventMouseButton and event.button_index == 1 and event.pressed

func is_touchtap(event):
	return event is InputEventScreenTouch

func is_click(event):
	return is_touchtap(event) or is_leftclick(event)

func _on_color_rect_gui_input(event):
	if is_click(event):
		Events.emit_signal("output_chosen", neighbor)
		queue_free()
