extends Control

@onready var grid_square_scene = load("res://grid_square/grid_square.tscn")
var color

func _ready():
	if not get_parent().get_parent().name == "Grid":
		self.visible = false
		
func _process(delta):
	for child in get_parent().get_children():
		if child.name.begins_with("Grid"):
			self.visible = false

func unsubdivide():
	var grid_node = get_parent()
	var grid_parent_node = grid_node.get_parent()
	var grid_instance = grid_square_scene.instantiate()
	grid_parent_node.add_child(grid_instance)
	
	# set position and size
	grid_instance.position = grid_node.position
	
	grid_node.queue_free()

func toggle_highlight(highlight_on):
	if highlight_on:
		color = $Sprite2D.modulate
		$Sprite2D.modulate = Color("ffffff", 0.9)
	else: $Sprite2D.modulate = color

func show_menu():
	hide_all_other_menus()
	if Events.can_tick: return
	$Menu.visible = true
	# choose which buttons to show.
	$Menu/HBox/UnSubdivide.visible = true

func hide_all_other_menus():
	for grid in get_tree().get_nodes_in_group("grid"):
		grid.get_node("Control").hide_menu()
	for square in get_tree().get_nodes_in_group("grid_square"):
		square.hide_menu()

func hide_menu():
	$Menu.visible = false

func _on_mouse_entered():
	toggle_highlight(true)

func _on_mouse_exited():
	toggle_highlight(false)

func is_leftclick(event):
	return event is InputEventMouseButton and event.button_index == 1 and event.pressed

func is_touchtap(event):
	return event is InputEventScreenTouch

func is_click(event):
	return is_touchtap(event) or is_leftclick(event)

func _on_input_event(viewport, event, shape_idx):
	if is_click(event):
		show_menu()

func _on_un_subdivide_gui_input(event):
	if is_click(event):
		hide_menu()
		unsubdivide()

func _on_gui_input(event):
	if is_click(event):
		show_menu()
