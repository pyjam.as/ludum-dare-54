extends Area2D

func is_leftclick(event):
	return event is InputEventMouseButton and event.button_index == 1 and event.pressed

func is_touchtap(event):
	return event is InputEventScreenTouch

func is_click(event):
	return is_touchtap(event) or is_leftclick(event)

func _on_input_event(viewport, event, shape_idx):
	if is_click(event):
		if not Events.can_tick:
			# check if player is allowed to click
			for machine in get_tree().get_nodes_in_group("machine"):
				if machine.has_deleted_output():
					return # TODO: warn user that this is why they can't start

		Events.can_tick = not Events.can_tick
		if Events.can_tick:
			$Sprite2D.texture = load("res://images/mandag/btn_restart.png")
			close_all_menus()

		else:
			$Sprite2D.texture = load("res://images/mandag/btn_play.png")
			remove_all_items()

func close_all_menus():
	for square in get_tree().get_nodes_in_group("grid_square"):
		square.hide_menu()
	for grid in get_tree().get_nodes_in_group("grid"):
		grid.get_node("Control").hide_menu()

func remove_all_items():
	for item in get_tree().get_nodes_in_group("item"):
		item.queue_free()
	for square in get_tree().get_nodes_in_group("grid_square"):
		square.reset_state()
