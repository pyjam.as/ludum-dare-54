{


  # Commit hash for nixos-unstable as of 2023-09-15
  # `git ls-remote https://github.com/nixos/nixpkgs nixos-unstable`
  inputs.nixpkgs.url =
    "github:NixOS/nixpkgs/f2ea252d23ebc9a5336bf6a61e0644921f64e67c";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # nixos 23.05
        pkgs = import nixpkgs {
          system = system;
          config.allowUnfree = false;
        };

        export_templates_tpz = pkgs.fetchurl {
          url =
            "https://github.com/godotengine/godot/releases/download/4.1.1-stable/Godot_v4.1.1-stable_export_templates.tpz";
          hash = "sha256-22BPoqPmcako7EmBmyJAet9chSKQZ4RXUktendyK2fU=";
        };

        export_templates = pkgs.stdenv.mkDerivation {
          name = "Godot 4.1.1-stable export templates";
          src = export_templates_tpz;
          nativeBuildInputs = [pkgs.unzip];
          dontUnpack = true;
          buildPhase = ''
            unzip $src
          '';
          installPhase = ''
            cp -r ./templates $out
          '';
        };

        game = pkgs.stdenv.mkDerivation {
          name = "Ludum Dare 54 Godot Web Export";
          src = ./.;
          nativeBuildInputs = [ pkgs.unzip pkgs.godot_4 ];
          buildPhase = ''
            # set up fake home for godot
            mkdir -p fakehome
            HOME=./fakehome

            # make dirs that need to be here
            # otherwise godot just crashes
            mkdir -p ./fakehome/.config/godot/feature_profiles
            mkdir -p ./fakehome/.cache/godot

            # move in export templates
            mkdir -p ./fakehome/.local/share/godot/export_templates/
            cp -r ${export_templates} ./fakehome/.local/share/godot/export_templates/4.1.1.stable

            # build game
            mkdir -p ./build
            godot4 --headless --export-debug "Web" ./build/index.html
          '';
          installPhase = ''
            mv ./build $out
          '';
        };

        caddyFile = httpPort: httpsPort: tlsblock: pkgs.writeText "Caddyfile" ''
          {
              http_port ${httpPort}
              https_port ${httpsPort}
          }

          https:// {
              ${tlsblock}
              header {
                  Cross-Origin-Opener-Policy same-origin
                  Cross-Origin-Embedder-Policy require-corp
              }
              root * ${game}
              file_server
          }
        '';

        tls_block = ''
              tls internal {
                  on_demand
              }
        '';

        serve_command = httpPort: httpsPort: tlsblock: ''
          ${pkgs.caddy}/bin/caddy run --adapter caddyfile --config ${caddyFile httpPort httpsPort tlsblock}
        '';

        serve = pkgs.writeShellScriptBin "serve" (serve_command "8080" "1337" tls_block);

        containerImage = pkgs.dockerTools.buildLayeredImage {
          name = "ldjam54";
          contents = [ ];
          config = {
            Cmd = [ "${pkgs.bash}/bin/bash" "-c" (serve_command "80" "443" "") ];
            ExposedPorts = {
              "80/tcp" = { };
              "443/tcp" = { };
            };
          };
        };
      in {
        packages.default = game;
        packages.container = containerImage;
        apps.default = flake-utils.lib.mkApp { drv = serve; };
        checks = {inherit game;};
      });
}
