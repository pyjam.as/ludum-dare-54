extends Node

var can_tick = false
signal tick

signal output_chosen(node: Node2D)

var counter = 0
var tick_length = 1

func _process(delta):
	counter += delta
	if counter > tick_length:
		counter -= tick_length
		
		if can_tick: 
			emit_signal("tick")
