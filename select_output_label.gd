extends Label

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	visible = get_node("../..").visible
	if not visible:
		return
	position = get_viewport_rect().size / 2
	position.x -= self.get_global_rect().size.x / 2
	position.y -= 200
